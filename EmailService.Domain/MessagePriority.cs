﻿namespace EmailService.Domain
{
    public enum MessagePriority
    {
        High,
        Normal,
        Low
    }
}