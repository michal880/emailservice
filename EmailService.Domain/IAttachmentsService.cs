﻿using System;
using System.IO;

namespace EmailService.Domain
{
    public interface IAttachmentsService
    {
        void StoreAttachment(Guid messageId, string attachmentName, Stream file);
        Stream RetrieveAttachment(Guid messageId, string attachmentName);
    }
}
