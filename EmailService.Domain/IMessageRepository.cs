﻿using System;
using System.Collections.Generic;

namespace EmailService.Domain
{
    public interface IMessageRepository
    {
        IEnumerable<Message> GetAll();
        Message Get(Guid id);
        void Add(Message message);
        void Update(Message message);

    }
}
