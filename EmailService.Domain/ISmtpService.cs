﻿using System.Collections.Generic;
using System.IO;

namespace EmailService.Domain
{
    public interface ISmtpService
    {
        void SendEmail(string sender, IEnumerable<string> recipientEmailAddresses, string messageSubject,
            string messageContent, MessagePriority messagePriority, IDictionary<string, Stream> attachments);
    }
}