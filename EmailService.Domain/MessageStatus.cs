﻿namespace EmailService.Domain
{
    public enum MessageStatus
    {
        Pending,
        Sent
    }
}