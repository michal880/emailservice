﻿using System;
using System.Collections.Generic;
using System.Xml.Xsl;

namespace EmailService.Domain
{
    public sealed class Message
    {
        private  List<string> _attachments;
        private  List<string> _recipients;
        public Message() {}
        public Message(Guid id, string sender, IEnumerable<string> recipientsEmailAddresses, string subject = "", string textContent = "", MessagePriority messagePriority = MessagePriority.Normal)
        {
            Id = id;
            Sender = sender;
            _attachments = new List<string>();
            Subject = subject;
            TextContent = textContent;
            Priority = messagePriority;
            Status = MessageStatus.Pending;
            _recipients = new List<string>();
            AddRecipients(recipientsEmailAddresses);
        }
        public Guid Id { get; private set; }

        public IEnumerable<string> Recipients => _recipients;

        public IEnumerable<string> Attachments => _attachments;

        public MessageStatus Status { get; private set; }
        public MessagePriority Priority { get; private set; }
        public string Sender { get; private set; }
        public string Subject { get; private set; }
        public string TextContent { get; private set; }
        public bool CannotBeChanged => Status == MessageStatus.Sent;

        public void AddRecipients(IEnumerable<string> recipientsEmailAddresses)
        {
            if (CannotBeChanged)
                throw new InvalidOperationException("Message has been already sent, cannot change it afterwards");

            if(recipientsEmailAddresses is null)
                throw new ArgumentException("Recipients email addresses cannot be null");

            foreach (var recipientEmailAddress in recipientsEmailAddresses)
            {
                AddRecipient(recipientEmailAddress);
            }
        }
        public void AddRecipient(string recipientEmailAddress)
        {
            if (CannotBeChanged)
                throw new InvalidOperationException("Message has been already sent, cannot change it afterwards");

            if (string.IsNullOrWhiteSpace(recipientEmailAddress))
                throw new ArgumentException("Email address cannot be null or whitespace");

            _recipients.Add(recipientEmailAddress);
        }
        public void MarkAsSent()
        {
            Status = MessageStatus.Sent;
        }

        public void AddAttachment(string attachmentName)
        {
            if (CannotBeChanged)
                throw new InvalidOperationException("Message has been already sent, cannot change it afterwards");
            _attachments.Add(attachmentName);
        }
    }
}
