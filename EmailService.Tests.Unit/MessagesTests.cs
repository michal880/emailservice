using EmailService.Domain;
using System;
using System.Collections.Generic;
using Xunit;

namespace EmailService.Tests.Unit
{
    public class MessagesTests
    {
        [Fact]
        public void CreateBasicMessageWithOneRecipient()
        {
            // Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> {"Test recipient"};
            // Act
            
            var message = new Message(expectedId, expectedSender, expectedRecipient);

            // Assert
            Assert.Equal(expectedId,message.Id);
            Assert.Equal(expectedSender,message.Sender);
            Assert.Equal(expectedRecipient, message.Recipients);
        }
        [Fact]
        public void CreateBasicMessageWithMultipleRecipients()
        {
            // Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> { "Test recipient1", "Test recipient2" };
            
            // Act

            var message = new Message(expectedId, expectedSender, expectedRecipient);

            // Assert
            Assert.Equal(expectedId, message.Id);
            Assert.Equal(expectedSender, message.Sender);
            Assert.Equal(expectedRecipient, message.Recipients);
        }
        [Fact]
        public void MarkMessageAsSentSetsStatusProperly()
        {
            // Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> { "Test recipient1", "Test recipient2" };
            var message = new Message(expectedId, expectedSender, expectedRecipient);
            var expectedStatus = MessageStatus.Sent;
            
            // Act
            message.MarkAsSent();

            // Assert
            Assert.Equal(expectedStatus,message.Status);
        }
        [Fact]
        public void CannotAddAttachmentsAfterMessageHasBeenSent()
        {
            // Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> { "Test recipient1", "Test recipient2" };
            var message = new Message(expectedId, expectedSender, expectedRecipient);
            
            void ThrowingMethod()
            {
                message.AddAttachment("newAttachment");
            }

            // Act
            message.MarkAsSent();
            
            // Assert
            Assert.Throws<InvalidOperationException>(ThrowingMethod);
        }

    }
}
