using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using EmailService.Application.Contracts;
using EmailService.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Xunit;

namespace EmailService.Tests.Api
{
    public class MessagesControllerTests : IClassFixture<TestWebApplicationFactory>
    {
        private HttpClient _client;

        public MessagesControllerTests(TestWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
        }
        [Fact]
        public void GetMessagesTest()
        {
            //Arrange
            
            var endpoint = "api/Messages";

            //Act
            var result = _client.GetAsync(endpoint).GetAwaiter().GetResult();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
        }
        [Fact]
        public void CreateEmailTest()
        {
            //Arrange
            var payload = new CreateMessageDto
                {Id = Guid.NewGuid(), Sender = "testuser@company.com", Recipients = new List<string> { "emailsendermr@gmail.com" }, Content = "Hello"};
            var endpoint = "api/Messages";
            
            //Act
            var result = _client.PostAsJsonAsync(endpoint, payload).GetAwaiter().GetResult();
            
            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
        }
        [Fact]
        public void GetMessageTest()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var payload = new CreateMessageDto
                { Id = expectedId, Sender = "testuser@company.com", Recipients = new List<string> { "emailsendermr@gmail.com" }, Content = "Hello" };
            var endpoint = "api/Messages";
            
            //Act
            _client.PostAsJsonAsync(endpoint, payload).GetAwaiter().GetResult();
            var result = _client.GetAsync($"{endpoint}/{expectedId}").GetAwaiter().GetResult();
            var content = result.Content.ReadAsAsync<ReadMessageDto>().GetAwaiter().GetResult();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
            Assert.NotNull(content);
            Assert.Equal(expectedId, content.Id);
            Assert.Equal(payload.Sender, content.Sender);
        }

        [Fact]
        public void GetDetailedMessageTest()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var payload = new CreateMessageDto
                { Id = expectedId, Sender = "testuser@company.com", Recipients = new List<string> { "emailsendermr@gmail.com" }, Content = "Hello" };
            var endpoint = "api/Messages";
            var expectedStatus = MessageStatus.Pending;
            var expectedAttachmentsCount = 0;

            //Act
            _client.PostAsJsonAsync(endpoint, payload).GetAwaiter().GetResult();
            var result = _client.GetAsync($"{endpoint}/{expectedId}/details").GetAwaiter().GetResult();
            var content = result.Content.ReadAsAsync<ReadDetailedMessageDto>().GetAwaiter().GetResult();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
            Assert.NotNull(content);
            Assert.Equal(expectedId, content.Id);
            Assert.Equal(payload.Sender, content.Sender);
            Assert.Equal(expectedStatus.ToString(), content.Status);
            Assert.Equal(expectedAttachmentsCount, content.Attachments?.Count());
        }

        [Fact]
        public void SendEmailTest()
        {
            //Arrange
            var payload = new CreateMessageDto
                { Id = Guid.NewGuid(), Sender = "testuser@company.com", Recipients = new List<string> { "emailsendermr@gmail.com" }, Content = "Hello" };
            var endpoint = "api/Messages";

            //Act
             _client.PostAsJsonAsync(endpoint, payload).GetAwaiter().GetResult();
            var result = _client.PostAsync($"{endpoint}/{payload.Id}/send",new StringContent("")).GetAwaiter().GetResult();
            
            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
        }
        [Fact]
        public void AddAttachmentTest()
        {
            //Arrange
            var payload = new CreateMessageDto
                { Id = Guid.NewGuid(), Sender = "testuser@company.com", Recipients = new List<string> { "emailsendermr@gmail.com" }, Content = "Hello" };
            var endpoint = "api/Messages";
            var content = new MultipartFormDataContent
            {
                {new StreamContent(GetSampleStream()), "attachment", "photo.jpg" }
            };

            //Act
            _client.PostAsJsonAsync(endpoint, payload).GetAwaiter().GetResult();
            var result = _client.PostAsync($"{endpoint}/{payload.Id}/attachments", content).GetAwaiter().GetResult();

            //Assert
            Assert.NotNull(result);
            Assert.True(result.IsSuccessStatusCode);
        }
        private MemoryStream GetSampleStream()
        {
            var base64Image =
                "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAE0lEQVR42mP8z8BQz4AGGGkgCADI1gd8Q+iZHQAAAABJRU5ErkJggg==";
            return new MemoryStream(Convert.FromBase64String(base64Image));
        }
    }
}
