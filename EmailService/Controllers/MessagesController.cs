﻿using System;
using System.Collections.Generic;
using EmailService.Application;
using EmailService.Application.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly MessageService _messageService;

        public MessagesController(MessageService messageService)
        {
            _messageService = messageService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var result = _messageService.GetAllMessages();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var result = _messageService.GetMessage(id);
            return Ok(result);
        }
        [HttpGet("{id}/details")]
        public IActionResult GetDetailed([FromRoute] Guid id)
        {
            var result = _messageService.GetDetailedMessage(id);
            return Ok(result);
        }
        [HttpPost]
        public IActionResult Create([FromBody] CreateMessageDto createMessageDto)
        {
            _messageService.CreateMessage(createMessageDto);
            return Created($"api/messages/{createMessageDto.Id}", createMessageDto.Id);
        }

        [HttpPost("{id}/attachments")]
        public IActionResult AddAttachment([FromRoute] Guid id, IFormFile attachment)
        {
            _messageService.AddAttachment(id, attachment.OpenReadStream(), attachment.FileName);
            return Accepted();
        }
        [HttpPost("{id}/send")]
        public IActionResult SendEmail([FromRoute] Guid id)
        {
            _messageService.SendMessage(id);
            return Accepted();
        }

    }
}
