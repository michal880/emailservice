﻿## API REST structure


#### GIVEN ACCEPTANCE CRITERIA

1. As a user, I can create a new email without sending it 
2. As a user, I can define one or many recipients 
3. As a user, I can define the sender
4. As a user, I can check the status of the email (pending, sent) 
5. As a user, I can see details of the email 
6. As a user, I can see all email in the system
7. As a user, I can send all pending email using company SMTP account 
8. (OPTIONAL) As a user, I can add attachments to the email 
9. (OPTIONAL) As a user, I can define the priority of the email


### REQUIRED
	
/emails
* [POST] criteria 1-3
* [GET] criteria 6

/emails/\{emailId}
* [GET] criteria 4


/emails/\{emailId}/details
* [GET] criteria 5

/emails/send
* [POST] criteria 7

### OPTIONAL

/emails/\{emailId}/attachments
* [POST] criteria 8

/emails
 * [POST] criteria 9 - just extend endpoint from criteria 1-3 with priority property


## ASSUMPTIONS

* Cloud based services are allowed to be used
* Email can be sent only once
* Sending all emails is possible, but by making a call each time to send one email
* SMTP account credentials are provided from application settings



## TRADE-OFFS

  * API was supposed to be RESTful, however, not all acceptance criteria can be fullfilled by exposing only resource endpoint - specifically sending the email. It is going to be achieved by exposting action method on a message resource, which is not fully REST compliant.

## Scalability

To adjust the microservice for bigger load, firstly we would need to know a little bit more about the load's characteristics.
* Load distribution - Are those milions of request distributed more or less evenly over the day, or just specific part of it, or maybe there are just random peaks.
* Load source - is the load coming from specific area of the world, or is it more geographically distributed?
* What are the infrastructural resources available? Is public cloud okay, or need to use only on premise solutions?
* Which requests are the most frequent ones? 

Leaving aside these questions, we have few options possible to adjust to bigger load, every one of them differs by the complexity that comes with it and addresses specific needs.
* making API asynchronous
* CQRS
* adding caching in application
* adding cdn
* scaling up
* scaling out (kubernetes/faas or more application nodes)
* db sharding/geo replication
