﻿using System;
using EmailService.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System.Collections.Generic;
using System.IO;
using EmailService.Domain;
using Xunit;

namespace EmailService.Tests.Integration
{
    public class SmtpServiceTests
    {
        private readonly IOptionsMonitor<SmtpConfiguration> _configuration;
        private readonly ILogger<SmtpService> _logger;

        public SmtpServiceTests()
        {
            var mock = new Mock<IOptionsMonitor<SmtpConfiguration>>();
            mock.Setup(m => m.CurrentValue).Returns(
                new SmtpConfiguration
                {
                    Host= "smtp.gmail.com",
                    Port= 587,
                    Username= "emailsendermr@gmail.com",
                    Password= "mailer587"
                });
            _configuration = mock.Object;
            _logger = new Mock<ILogger<SmtpService>>().Object;
        }

        [Fact]
        public void SendingEmailShouldNotThrowExceptions()
        {
            //Arrange
            var smtpService = new SmtpService(_configuration, _logger);
            var sender = "person@company.com";
            var recipients = new List<string> {"emailsendermr@gmail.com"};
            var subject = string.Empty;
            var messageContent = string.Empty;
            var messagePriority = MessagePriority.Normal;
            var attachments = new Dictionary<string, Stream> {{"sample.jpg", GetSampleStream()}};

            //Act
            smtpService.SendEmail(sender, recipients, subject, messageContent, messagePriority, attachments);

            //Assert
            

        }
        private MemoryStream GetSampleStream()
        {
            var base64Image =
                "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAE0lEQVR42mP8z8BQz4AGGGkgCADI1gd8Q+iZHQAAAABJRU5ErkJggg==";
            return new MemoryStream(Convert.FromBase64String(base64Image));
        }
    }
}
