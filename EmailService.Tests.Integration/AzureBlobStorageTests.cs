using System;
using System.IO;
using EmailService.Infrastructure;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace EmailService.Tests.Integration
{
    public class AzureBlobStorageTests 
    {
        private readonly IOptionsMonitor<AzureBlobStorageConfiguration> _configuration;

        public AzureBlobStorageTests() 
        {
            var mock = new Mock<IOptionsMonitor<AzureBlobStorageConfiguration>>();
            mock.Setup(m => m.CurrentValue).Returns(
                new AzureBlobStorageConfiguration
                {
                    StorageAccountConnectionString = "DefaultEndpointsProtocol=https;AccountName=emailservicehwai03xd;AccountKey=QYmaaXWhNV9z0u1bWnXfG6oKKvTwWFiVktEl/C0wrsj3MWeYzo9yVFmNMbKgD/iSQ92N2iM8A2ntJGVv0XDoBg==;EndpointSuffix=core.windows.net",
                    ContainerName= "messageattachments"
                });
            _configuration = mock.Object;
        }
        [Fact]
        public void UploadingImageShouldNotThrowAnyExceptions()
        {
            //Arrange
            var sut = new AzureBlobStorageService(_configuration);
            using var payload = GetSampleStream();
            var messageId = Guid.NewGuid();
            var fileName = "TestFile";
            //Act

            sut.StoreAttachment(messageId, fileName, payload);

            //Assert

        }

        [Fact]
        public void UploadedImageShouldBePossibleToRetrieve()
        {
            //Arrange
            var sut = new AzureBlobStorageService(_configuration);
            using var payload = GetSampleStream();
            var messageId = Guid.NewGuid();
            var fileName = "TestFile";
            //Act

            sut.StoreAttachment(messageId, fileName, payload);
            using var result = sut.RetrieveAttachment(messageId, fileName);

            //Assert
            Assert.NotNull(result);
            Assert.True(result.CanRead);


        }

        private MemoryStream GetSampleStream()
        {
            var base64Image =   
                "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAE0lEQVR42mP8z8BQz4AGGGkgCADI1gd8Q+iZHQAAAABJRU5ErkJggg==";
            return new MemoryStream(Convert.FromBase64String(base64Image));
        }
    }
}
