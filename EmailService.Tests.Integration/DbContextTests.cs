﻿using System;
using System.Collections.Generic;
using System.Linq;
using EmailService.Domain;
using EmailService.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace EmailService.Tests.Integration
{
    public class DbContextTests
    {
        private EmailServiceContext _emailServiceContext;

        public DbContextTests()
        {
            // Don't do this at home
            const string connectionString = "Server=tcp:emailservice-s49vwtoy.database.windows.net,1433;Initial Catalog=emailservice;Persist Security Info=False;User ID=dbadmin;Password=406ts2Ud;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

            var optionsBuilder = new DbContextOptionsBuilder<EmailServiceContext>();
            optionsBuilder.UseSqlServer(connectionString);
            _emailServiceContext = new EmailServiceContext(optionsBuilder.Options);
        }

        [Fact]
        public void CanConnectToDb()
        {
            //Arrange
            
            //Act
            _emailServiceContext.Database.GetPendingMigrations();

            //Assert


        }

        [Fact]
        public void CanPersistEntity()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> { "Test recipient" };
            var message = new Message(expectedId, expectedSender, expectedRecipient);
            var expectedChanges = 1;
            //Act
            _emailServiceContext.Messages.Add(message);
            var result = _emailServiceContext.SaveChanges();

            //Assert
            Assert.Equal(expectedChanges, result);

        }
        [Fact]
        public void CanPersistAndReadEntity()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var expectedSender = "Test sender";
            var expectedRecipient = new List<string> { "Test recipient" };
            var message = new Message(expectedId, expectedSender, expectedRecipient);

            //Act
            _emailServiceContext.Messages.Add(message);
            _emailServiceContext.SaveChanges();

            var result = _emailServiceContext.Messages.Single(e => e.Id == expectedId);

            //Assert
            Assert.Equal(expectedId, result.Id);
            Assert.Equal(expectedSender, message.Sender);
            Assert.Equal(expectedRecipient.FirstOrDefault(), message.Recipients.FirstOrDefault());
            

        }
    }
}
