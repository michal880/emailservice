﻿using System;
using EmailService.Application.Contracts;
using EmailService.Domain;
using System.Collections.Generic;
using System.IO;

namespace EmailService.Application
{
    public class MessageService
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IAttachmentsService _attachmentsService;
        private readonly ISmtpService _smtpService;
        private readonly IUnitOfWork _unitOfWork;

        public MessageService(IMessageRepository messageRepository, IAttachmentsService attachmentsService, ISmtpService smtpService, IUnitOfWork unitOfWork)
        {
            _messageRepository = messageRepository;
            _attachmentsService = attachmentsService;
            _smtpService = smtpService;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ReadMessageDto> GetAllMessages()
        {
            var messages = _messageRepository.GetAll();
            foreach (var message in messages)
            {
                yield return MapEntityToDto(message);
            }
        }
        public ReadMessageDto GetMessage(Guid id)
        {
            var message = _messageRepository.Get(id);
            return MapEntityToDto(message);
        }
        public ReadDetailedMessageDto GetDetailedMessage(Guid id)
        {
            var message = _messageRepository.Get(id);

            return MapEntityToDetailedDto(message);
        }

        public void CreateMessage(CreateMessageDto createMessageDto)
        {
            Enum.TryParse(createMessageDto.Priority,true, out MessagePriority priority);
            var message = new Message(createMessageDto.Id, createMessageDto.Sender, createMessageDto.Recipients, textContent:createMessageDto.Content, messagePriority: priority);
            _messageRepository.Add(message);
            _unitOfWork.Commit();
        }

        public void AddAttachment(Guid messageId, Stream attachment, string attachmentName)
        {
            var message = _messageRepository.Get(messageId);
            message.AddAttachment(attachmentName);
            _attachmentsService.StoreAttachment(messageId, attachmentName, attachment);
            _messageRepository.Update(message);
            _unitOfWork.Commit();
            attachment.Dispose();
        }

        public void SendMessage(Guid messageId)
        {
            var message = _messageRepository.Get(messageId);
            var attachmentsStreams = new Dictionary<string, Stream>();
            foreach (var messageAttachmentName in message.Attachments)
            {
                attachmentsStreams.Add(messageAttachmentName, _attachmentsService.RetrieveAttachment(message.Id, messageAttachmentName));
            }
            _smtpService.SendEmail(message.Sender, message.Recipients, message.Subject, message.TextContent, message.Priority, attachmentsStreams);
            message.MarkAsSent();
            _messageRepository.Update(message);
            _unitOfWork.Commit();
        }

        private static ReadMessageDto MapEntityToDto(Message messageEntity)
        {
            return new ReadMessageDto
            {
                Id = messageEntity.Id, 
                Sender = messageEntity.Sender
            };
        }
        private static ReadDetailedMessageDto MapEntityToDetailedDto(Message messageEntity)
        {
            return new ReadDetailedMessageDto
            {
                Id = messageEntity.Id,
                Sender = messageEntity.Sender,
                Content = messageEntity.TextContent,
                Priority = messageEntity.Priority.ToString(),
                Status = messageEntity.Status.ToString(),
                Recipients = messageEntity.Recipients,
                Attachments = messageEntity.Attachments,
            };
        }
    }
}
