﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailService.Application
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
