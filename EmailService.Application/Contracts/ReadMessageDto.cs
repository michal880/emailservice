﻿using System;

namespace EmailService.Application.Contracts
{
    public class ReadMessageDto
    {
        public Guid Id { get; set; }
        public string Sender { get; set; }
    }
}
