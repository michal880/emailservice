﻿using System;
using System.Collections.Generic;

namespace EmailService.Application.Contracts
{
    public class ReadDetailedMessageDto
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string Sender { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public IEnumerable<string> Recipients { get; set; }
        public IEnumerable<string> Attachments { get; set; }

    }
}
