﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace EmailService.Application.Contracts
{
    public class CreateMessageDto
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string Sender { get; set; }

        public string Priority { get; set; }

        public IEnumerable<string> Recipients { get; set; }
    }
}
