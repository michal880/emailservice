﻿using EmailService.Domain;
using Microsoft.WindowsAzure.Storage;
using System;
using System.IO;
using Microsoft.Extensions.Options;

namespace EmailService.Infrastructure
{
    public class AzureBlobStorageService : IAttachmentsService
    {
        private readonly AzureBlobStorageConfiguration _configuration;
        public AzureBlobStorageService(IOptionsMonitor<AzureBlobStorageConfiguration> configuration)
        {
            _configuration = configuration.CurrentValue;
        }

        public void StoreAttachment(Guid messageId, string attachmentName, Stream file)
        {
            var accountConnection = _configuration.StorageAccountConnectionString;

            var storageAccount = CloudStorageAccount.Parse(accountConnection);

            var blobClient = storageAccount.CreateCloudBlobClient();

            var container = blobClient.GetContainerReference(_configuration.ContainerName);

            var blockBlob = container.GetBlockBlobReference(messageId + attachmentName);

            blockBlob.UploadFromStreamAsync(file).GetAwaiter().GetResult();
        }
        
        public Stream RetrieveAttachment(Guid messageId, string attachmentName)
        {
            var accountConnection = _configuration.StorageAccountConnectionString;

            var storageAccount = CloudStorageAccount.Parse(accountConnection);

            var blobClient = storageAccount.CreateCloudBlobClient();

            var shareReference = _configuration.ContainerName;

            var container = blobClient.GetContainerReference(shareReference);

            var blockBlob = container.GetBlockBlobReference(messageId + attachmentName);

            return blockBlob.OpenReadAsync().GetAwaiter().GetResult();

        }
    }
}
