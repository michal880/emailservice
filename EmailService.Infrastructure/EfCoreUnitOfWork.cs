﻿using System;
using System.Collections.Generic;
using System.Text;
using EmailService.Application;

namespace EmailService.Infrastructure
{
    public class EfCoreUnitOfWork : IUnitOfWork
    {
        private readonly EmailServiceContext _dbContext;

        public EfCoreUnitOfWork(EmailServiceContext dbContext)
            => _dbContext = dbContext;
        public void Commit() => _dbContext.SaveChanges();
    }
}
