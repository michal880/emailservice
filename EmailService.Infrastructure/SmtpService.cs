﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using EmailService.Domain;

namespace EmailService.Infrastructure
{
    public class SmtpService : ISmtpService
    {
        private readonly SmtpConfiguration _config;
        private readonly ILogger<SmtpService> _logger;

        public SmtpService(IOptionsMonitor<SmtpConfiguration> config, ILogger<SmtpService> logger)
        {
            _config = config.CurrentValue;
            _logger = logger;
        }
        public void SendEmail(string sender, IEnumerable<string> recipientEmailAddresses, string messageSubject,
            string messageContent, MessagePriority messagePriority, IDictionary<string, Stream> attachments)
        {
            var messages = new List<MailMessage>();
            foreach (var recipientEmailAddress in recipientEmailAddresses)
            {
                var message = BuildMessage(sender, recipientEmailAddress, messageSubject, messageContent, messagePriority, attachments);
                messages.Add(message);
            }
            var client = CreateClient();
            try
            {
                foreach (var message in messages)
                {
                    client.Send(message);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Issue occured while sending an email");
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
        private SmtpClient CreateClient()
        {
            var client = new SmtpClient(_config.Host, _config.Port)
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_config.Username, _config.Password),
            };
            return client;
        }

        private static MailMessage BuildMessage(string sender, string recipientEmailAddress, string messageSubject,
            string messageBody, MessagePriority messagePriority, IDictionary<string, Stream> attachments)
        {
            var message = new MailMessage(sender, recipientEmailAddress, messageSubject, messageBody);
            foreach (var attachment in attachments)
            {
                message.Attachments.Add(new Attachment(attachment.Value, attachment.Key));
            }
            message.Priority = MapPriority(messagePriority);
            return message;
        }

        private static MailPriority MapPriority(MessagePriority priority)
        {
            switch (priority)
            {
                case MessagePriority.High:
                    return MailPriority.High;
                case MessagePriority.Normal:
                    return MailPriority.Normal;
                case MessagePriority.Low:
                    return MailPriority.Low;
                default:
                    throw new NotImplementedException("Unknown message priority");
            }
        }
    }
}