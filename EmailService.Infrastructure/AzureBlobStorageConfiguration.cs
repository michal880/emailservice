﻿namespace EmailService.Infrastructure
{
    public class AzureBlobStorageConfiguration
    {
        public string ContainerName { get; set; }

        public string StorageAccountConnectionString { get; set; }
    }
}
