﻿using System.Collections.Generic;
using EmailService.Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EmailService.Infrastructure
{
    public class EmailServiceContext : DbContext
    {
        public EmailServiceContext(DbContextOptions<EmailServiceContext> options) : base(options)
        {
        }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>().Property(x => x.Attachments).HasConversion(@out => JsonConvert.SerializeObject(@out), @in => JsonConvert.DeserializeObject<List<string>>(@in));
            modelBuilder.Entity<Message>().Property(x => x.Recipients).HasConversion(@out => JsonConvert.SerializeObject(@out), @in => JsonConvert.DeserializeObject<List<string>>(@in));
        }
    }
}