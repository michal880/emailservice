﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Infrastructure
{
    public static class PersistenceExtension
    {
        public static void ConfigurePersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<EmailServiceContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("Database"),
                    ctx => ctx.MigrationsAssembly("EmailService.Infrastructure")
                        .EnableRetryOnFailure(3)));
        }
    }
}
