﻿using EmailService.Domain;
using System;
using System.Collections.Generic;
using System.Linq;


namespace EmailService.Infrastructure
{
    public class MessageRepository : IMessageRepository
    {
        private readonly EmailServiceContext _context;

        public MessageRepository(EmailServiceContext context)
        {
            _context = context;
        }
        public IEnumerable<Message> GetAll()
        {
            return _context.Messages.AsEnumerable();
        }

        public Message Get(Guid id)
        {
            return _context.Messages.Single(q => q.Id.Equals(id));
        }

        public void Add(Message message)
        {
            _context.Messages.Add(message);
        }

        public void Update(Message message)
        {
            _context.Messages.Update(message);
        }

    }
}
